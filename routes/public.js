const express = require('express');
const router = express.Router();

const bcrypt = require('../utils/bcrypt')
const Token = require('../utils/token')
const jwt = require('jsonwebtoken')

const db = require('../models')

/* ALL request to / redirect to home */
router.all('/', (req, res, next) => {
    console.log('Redirection to home page');
    //next();
    res.redirect('/home');
})

/* GET home page. */
router.get('/home', (req, res, next) => {
    res.render('index', {
        session: req.session,
        user: req.user,
        title: 'Home'
    })
});

/* GET troops page */
router.get('/troops', async (req, res, next) => {

    let options = {}

    if (req.query.limit) {
        req.query.offset = req.query.offset ? req.query.offset : 0

        options.limit = req.query.limit
        options.offset = req.query.offset
    }

    const cards = await db.Card.findAndCountAll(options);

    res.format({
        html: () => {
            res.render('troops/all', {
                session: req.session,
                user: req.user,
                title: 'Troupes',
                cards: troops.rows
            })
        },

        json: () => {
            res.json(troops)
        }
    });
})

/* GET login page */
router.get('/login', (req, res, next) => {
    res.render('user/login', {
        session: req.session,
        user: req.user,
        title: 'Login'
    })
})

/* POST login */
router.post('/login', async (req, res, next) => {
    try {
        if (!req.body.username || !req.body.password) {
            throw new Error('Il manque des informations')
        }

        var user = await db.User.findOne({
            where: {
                username: req.body.username
            }
        })

        if (!user) {
            throw new Error('Utilisateur inconnu')
        }

        user = user.dataValues

        const isValidPassword = await bcrypt.compare(req.body.password, user.password)

        if (!isValidPassword) {
            throw new Error('Mot de passe invalide')
        }

        const token = await Token.getRandom()
        const now = new Date()
        let expireDate = new Date()

        expireDate.setHours(now.getHours() + 1)

        await db.Session.create({
            accessToken: token,
            userId: user.id,
            expiresAt: expireDate
        })

        res.format({

            html: () => {
                res.cookie('AccessToken', token, {
                    maxAge: 900000,
                    httpOnly: true
                })
                res.redirect('/home')
            },

            json: () => {
                res.json({
                    accessToken: token
                })
            }
        })
    } catch (Err) {
        next(Err)
    }
})

/* GET register page */
router.get('/register', (req, res, next) => {
    res.render('user/register', {
        session: req.session,
        user: req.user,
        title: 'Register',
        isNew: true
    })
})

/* POST register */
router.post('/register', async (req, res, next) => {
    try {
        if (req.body.username && req.body.firstname && req.body.lastname && req.body.password && req.body.confirmPassword) {

            if (req.body.password === req.body.confirmPassword) {
                hashedPassword = await bcrypt.hash(req.body.password)
            } else {
                throw new Error('Le mot de passe ne correspond pas à la confirmation de mot de passe')
            }

        } else {
            throw new Error('Il manque des informations')
        }

        const alreadyTaken = await db.User.findOne({
            where: {
                username: req.body.username
            }
        })

        if (alreadyTaken) {
            throw new Error("Nom d'utilisateur déjà pris")
        }

        const user = await db.User.create({
            username: req.body.username,
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            password: hashedPassword
        })

        res.format({

            html: () => {
                res.redirect('/login')
            },

            json: () => {
                res.json(user)
            }
        })

    } catch (Err) {
        next(Err)
    }
})

module.exports = router;