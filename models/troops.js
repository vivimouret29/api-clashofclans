'use strict';
module.exports = (sequelize, DataTypes) => {
  const troops = sequelize.define('troops', {
    title: DataTypes.STRING
  }, {});
  troops.associate = function(models) {
    // associations can be defined here
  };
  return troops;
};