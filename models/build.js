'use strict';
module.exports = (sequelize, DataTypes) => {
  const build = sequelize.define('build', {
    title: DataTypes.STRING
  }, {});
  build.associate = function(models) {
    // associations can be defined here
  };
  return build;
};