'use strict';
module.exports = (sequelize, DataTypes) => {
  const Construction = sequelize.define('Construction', {
    name: DataTypes.STRING,
    function: DataTypes.STRING,
    life_point: DataTypes.STRING,
    construction_time_sec: DataTypes.STRING
  }, {});
  Construction.associate = function(models) {
    // associations can be defined here
  };
  return Construction;
};