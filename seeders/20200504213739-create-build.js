'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('City Hall', [{
        id: 1,
        level: 1,
        life_point: 450,
        construction_time: N/A,
      },
      {
        id: 2,
        level: 1,
        life_point: 1.600,
        construction_time_sec: 10,
      },
      {
        id: 3,
        level: 1,
        life_point: 1.850,
        construction_time_sec: 7200,
      },
      {
        id: 4,
        level: 1,
        life_point: 2.100,
        construction_time_sec: 28800,
      },
      {
        id: 5,
        level: 1,
        life_point: 2.400,
        construction_time_sec: 43200,
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('City Hall', [{
        id: 1
      }, 
      {
        id: 2
      }, 
      {
        id: 3
      }, 
      {
        id: 4
      }, 
      {
        id: 5
      }], {});
  }
};
