'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('Troops', [{
        name: 'barbaric',
        level: 1,
        damage_per_seconds: 8,
        training_time_sec: 5,
      },
      {
        name: 'archer',
        level: 1,
        damage_per_seconds: 7,
        training_time_sec: 6,
      },
      {
        name: 'giant',
        level: 1,
        damage_per_seconds: 11,
        training_time_sec: 30,
      },
      {
        name: 'balloon',
        level: 1,
        damage_per_seconds: 25,
        training_time_sec: 30,
      },
      {
        name: 'wizard',
        level: 1,
        damage_per_seconds: 50,
        training_time_sec: 30,
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('Troops', [{
        name: 'barbaric',
      }, 
      {
        name: 'archer',
      }, 
      {
        name: 'giant',
      }, 
      {
        name: 'balloon',
      }, 
      {
        name: 'wizard',
      }], {});
  }
};