# API_ClashOfClans

## Lancement du projet en local

Une fois le projet cloner, utilisez ces commandes :
```
npm install

npm start
```

Vous pouvez maintenant vous rendre sur `http://localhost:3000/` pour accèder à l'application.  
  
Utilisez ces commandes pour effectuer les migrations et ainsi réinitialiser la base de données :
* avec `node_modules/.bin/sequelize db:migrate` pour lancer les migrations
* et `node_modules/.bin/sequelize db:migrate:undo:all` pour les annuler.

